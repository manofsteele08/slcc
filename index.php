<!DOCTYPE html>
<html>

<head>
    <title>About Me!</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="styles\mystyle.css">

    <!-- JS Links -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js"></script>
    <script src="scripts\main.js"></script>
</head>

<body>
    <?php include "nav.php"; ?>
    <div class="parallax">
        <div class="container">
            <div class="row">
                <div class="col" id="bio">
                    <h3>Personal Bio</h3>
                    <p>
                        Christopher Steele, who also goes by Chris, has a B.S. in Management Information Systems from
                        Utah State University and upon graduating started working for MasterControl in Cottonwood
                        Heights, UT. After starting at MasterControl, he worked on a Masters Degree in Information
                        Systems with a focus on Software Development at the University of Utah. He has worked as a
                        Software Quality Assurance Engineer and a Software Engineer with the latter being his current
                        role at the aforementioned company. He enjoys many out door things like hiking, canyoneering,
                        scuba diving and more. Indoor things are enjoyed as well like watching movies and playing video
                        games. Most important to him is his wife and two kids and spending time with them. He is
                        excited to share his knowledge and experiences in the classroom.
                    </p>
                </div>
            </div>
            <div class="row">
                <div class="col-sm">
                    <h3>Education</h3>
                    <p>
                        2016 - 2017 University of Utah, Salt Lake City, UT USA<br>
                        <em>M.S. in Informations Systems</em><br>
                        Software Development Emphasis
                        <br><br>
                        2011 - 2016 Utah State University, Logan, UT USA<br>
                        <em>B.S. in Management Information Systems</em><br>
                        Computer Science Minor
                    </p>
                </div>
                <div class="col-sm">
                    <h3>Experience</h3>
                    <p>
                        <?php
                            function buildString($skillSets) {
                                $result = "";
                                foreach($skillSets as $skillSet){
                                    $result .= $skillSet.", ";
                                }
                                return rtrim($result, ", ");
                            }

                            $result = "";
                            $skills = ["Java", "Cold Fusion", "Ruby", "Python", "C#"];
                            $hobbies = array("Watch Movies", "Like to code", "Make videos/VFX");

                            echo "<h4>Skills</h4>";
                            echo buildString($skills);
                            echo "<br>";
                            echo "<h4>Hobbies</h4>";
                            echo buildString($hobbies);


                        ?>
                    </p>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <h3>Work Experience</h3>
                    <p>
                        06/2017 - Present MasterControl, INC., Cottonwood Heights, UT USA<br>
                        <em>Software Engineer</em><br>
                        Currently I am a software engineer on our Enterprise Triage and Development team. We work
                        closely with our Technical Support and Engineering departments and oversee top critical
                        customer defects as well as other non critical defects.
                        Responsibilities include:
                        <ul>
                            <li>Reproducing and debugging using IDE's and debugging tools for applicable defects</li>
                            <li>Providing code fixes that will be code reviewed, tested and validated</li>
                            <li>Pair programming with team mates and providing code reviews for others</li>
                            <li>Providing patches for currently supported versions of our software</li>
                            <li>Providing on-call engineering support for our installation team during customer
                                upgrades</li>
                        </ul>
                        <br>
                        05/2016 - 06/2017 MasterControl, INC., Cottonwood Heights, UT USA<br>
                        <em>Software Quality Engineer</em><br>
                        <ul>
                            <li>Created and maintained scripts in Ruby that interfaced with Selenium Webdriver as part
                                of our
                                testing and software validation processes.</li>
                            <li>Manual testing of new features and enhancements as well as testing in accordance to our
                                functional and non-functional requirements sheets.</li>
                            <li>Documented our actions and results using Bugzilla and VersionOne</li>
                        </ul>
                    </p>
                </div>
            </div>

            <div class="row">
                <div class="col-sm">
                    <h3>Projects</h3>
                    <p>
                        Designed and built an office item management and check in/out system using Java and SQL.
                    </p>
                </div>
                <div class="col-sm">
                    <h3>Spoken Languages</h3>
                    <p>
                        English
                        Bahasa Malay
                    </p>
                </div>
                <div class="col-sm">
                    <h3>References</h3>
                    <p>
                        Mike<br>
                        Jason<br>
                        Bill<br>
                    </p>
                </div>
            </div>
        </div>
    </div>

</body>

</html>