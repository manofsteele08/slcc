<?php

// create_table();
// drop_table();
// get_content();

function get_db_connection(){
    $servername = "crl.wno.mybluehost.me";
    $username = "crlwnomy_redwood";
    $db_name = "crlwnomy_redwood";
    $password = "Redwood2019";

    try {
        $conn = new PDO("mysql:host=$servername;dbname=$db_name", $username, $password);
        // set the PDO error mode to exception
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        echo "Connected successfully"; 
    } catch(PDOException $e){
        echo "Connection failed: " . $e->getMessage();
    }

    return $conn;
}

function create_table(){
    $conn = get_db_connection();
    try {
        // sql to create table
        $sql = "CREATE TABLE cs_contact_responses (
                id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY, 
                date TIMESTAMP,
                first_name VARCHAR(255),
                last_name VARCHAR(255),
                email VARCHAR(255),
                phone VARCHAR(255),
                company VARCHAR(255),
                contact_reason VARCHAR(255),
                contact_message VARCHAR(255)
            )";
    
        // use exec() because no results are returned
        $conn->exec($sql);
        echo "Table contact_responses created successfully";
    } catch(PDOException $e) {
        echo "Failed to create table: " . $e->getMessage();
    }
    $conn = null;
}

function get_content(){
    $conn = get_db_connection();
    try{
        $stmt = $conn->prepare("SELECT * FROM csteele_contact_responses");
        $stmt->execute();
        // return $stmt->setFetchMode(PDO::FETCH_ASSOC);
        // set the resulting array to associative
        $result = $stmt->setFetchMode(PDO::FETCH_ASSOC);
        // print_r($stmt->fetchAll());

        foreach($stmt->fetchAll() as $row) { 
            foreach($row as $data){
                echo '<br>'.$data;
            }
        }

    } catch (PDOException $e) {
        echo "Error: " . $e->getMessage();
    }
}

function contact_form_insert($date, $fname, $lname, $email, $phone, $company, $reason, $message){
    $conn = get_db_connection();
    try{
        $sql = "INSERT INTO contact_responses (
            date, first_name, last_name, email, phone, company, contact_reason, contact_message
            ) VALUES (
            NOW(), '$fname', '$lname', '$email', '$phone', '$company', '$reason', '$message'
            )";

        $conn->exec($sql);
        echo "<br>Inserted Successfully!";
    } catch (PDOException $e){
        echo $sql . "<br>" . $e->getMessage();
    }
    $conn = null;
}

function drop_table(){
    $conn = get_db_connection();
    try {
        // sql to create table
        $sql = "DROP TABLE contact_responses;";
    
        // use exec() because no results are returned
        $conn->exec($sql);
        echo "Table contact_responses deleted successfully";
    } catch(PDOException $e) {
        echo "<br>" . $e->getMessage();
    }
    $conn = null;
}

?>