<?php
$tableName = "csteele_contact_responses";
// get_db_connection();
// create_table();
get_content();

function get_db_connection(){
    $serverName = "crl.wno.mybluehost.me";
    $databaseName = "crlwnomy_redwood";
    $username = "crlwnomy_redwood";
    $password = "Redwood2019";

    try {
         $conn = new PDO("mysql:host=$serverName;dbname=$databaseName", $username, $password);
         echo "Connected successfully!";
    } catch(PDOException $e){
        echo "Database connection failed: ".$e->getMessage();
    }

    return $conn;
}

function create_table(){
    $conn = get_db_connection();

    try {
        $sql = "CREATE TABLE $tableName (
            id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
            date TIMESTAMP,
            first_name VARCHAR(255),
            last_name VARCHAR(255),
            email VARCHAR(255),
            phone VARCHAR(255),
            company VARCHAR(255),
            contact_reason VARCHAR(255),
            contact_message VARCHAR(255)
            )";

        $conn->exec($sql);
        echo "Table $tableName created successfully";

    } catch(PDOException $e){
        echo "Could not create table: ".$e->getMessage();
    }

    $conn = null;
}

function insert_to_contact_table($date, $first_name, $last_name, $email, $phone, $company, $contact_reason, $contact_message){
    $conn = get_db_connection();

    try{
        $sql = "INSERT INTO $tableName 
        (date, first_name, last_name, email, phone, company, contact_reason, contact_message)
        VALUES
        ($date, '$first_name', '$last_name', '$email', '$phone', '$company', '$contact_reason', '$contact_message')";

        $conn->exec($sql);
        echo "Inserted successfully!";
    } catch(PDOException $e){
        echo "Error inserting into database: ".$e->getMessage();
    }

    $conn = null;
}

function get_content(){
    $conn = get_db_connection();
    try{
        $stmt = $conn->prepare("SELECT * FROM csteele_contact_responses");
        $stmt->execute();
        // return $stmt->setFetchMode(PDO::FETCH_ASSOC);
        // set the resulting array to associative
        $result = $stmt->setFetchMode(PDO::FETCH_ASSOC);
        // print_r($stmt->fetchAll());

        foreach($stmt->fetchAll() as $row) { 
            foreach($row as $data){
                echo '<br>'.$data;
            }
        }

    } catch (PDOException $e) {
        echo "Error: " . $e->getMessage();
    }
}
?>