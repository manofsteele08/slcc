<!DOCTYPE html>
<html>

<head>
    <title>About You</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="styles\mystyle.css">

    <!-- JS Links -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js"></script>
    <script src="scripts\main.js"></script>
</head>

<body>
    <?php include "nav.php"; ?>
    <div class="parralax-aboutyou">
        <div class="container">
            <div class="alert alert-danger alert-dismissible fade show d-none">
                <button type="button" class="close" onclick="closeAlert();">&times;</button>
                <strong>Stop!</strong> Please fill out all of the fields correctly.
            </div>
            <div class="row">
                <div class="col">
                    <h2 style="text-align: center;">Now tell me a bit about yourself!</h2>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <form name="aboutYouForm" method="post" onsubmit="event.preventDefault(); validateAbout();">
                        <div class="form-group">
                            <label for="fname">First Name</label>
                            <input type="text" class="form-control" id="fname" aria-describedby="firstName" placeholder="Enter First Name">
                        </div>
                        <div class="form-group">
                            <label for="lname">Last Name</label>
                            <input type="text" class="form-control" id="lname" aria-describedby="lastName" placeholder="Enter Last Name">
                        </div>
                        <div class="form-group">
                            <label for="txtHobbies">Hobbies</label>
                            <textarea class="form-control" id="txtHobbies" aria-describedby="txtHobbies" placeholder="Enter hobbies..."></textarea>
                        </div>
                        <div class="form-group">
                            <label for="txtMovies">Favorite Movie(s)</label>
                            <textarea class="form-control" id="txtMovies" aria-describedby="txtMovies" placeholder="Enter movies..."></textarea>
                        </div>
                        <div class="form-group">
                            <label for="txtFood">Favorite Food(s)</label>
                            <textarea class="form-control" id="txtFood" aria-describedby="txtFood" placeholder="Enter food..."></textarea>
                        </div>
                        <div class="form-group">
                            <label for="listDay">Favorite Day of The Week</label>
                            <select class="form-control" id="listDay">
                                <option value="monday">Monday</option>
                                <option value="tuesday">Tuesday</option>
                                <option value="wednesday">Wednesday</option>
                                <option value="thursday">Thursday</option>
                                <option value="friday">Friday</option>
                                <option value="saturday">Saturday</option>
                                <option value="sunday">Sunday</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label id="sportsLabel" for="radioSports">Like to watch Sports?</label><br>
                            <input type="radio" name="sports" value="professional">Professional
                            <input type="radio" name="sports" value="college">College
                            <input type="radio" name="sports" value="both">Both
                            <input type="radio" name="sports" value="none">None
                            <input type="radio" name="sports" value="other">Other
                        </div>
                        <label id="checkboxGames" for="checkGames">Gamer? (check all platforms that apply to you)</label><br>
                        <div class="form-check" id="checkGames">
                            <input type="checkbox" class="form-check-input" name="games" value="pc">PC<br>
                            <input type="checkbox" class="form-check-input" name="games" value="ps4">Playstation<br>
                            <input type="checkbox" class="form-check-input" name="games" value="xbox">Xbox<br>
                            <input type="checkbox" class="form-check-input" name="games" value="nintendo">Nintendo<br>
                            <input type="checkbox" class="form-check-input" name="games" value="classic">Classics
                            (Sega, Dreamcast, etc.)<br>
                            <input type="checkbox" class="form-check-input" name="games" value="phone">Smartphone<br>
                            <input type="checkbox" class="form-check-input" name="games" value="boardgames">Non-digital
                            (Monopoly, Risk, Dungeons and
                            Dragons, etc)<br>
                            <input type="checkbox" class="form-check-input" name="games" value="book">A good book is
                            all I need<br>
                        </div>
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</body>

</html>