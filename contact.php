<!DOCTYPE html>
<html>

<head>
    <title>Contact Me</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="styles\mystyle.css">

    <!-- JS Links -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js"></script>
    <script src="scripts\main.js"></script>
</head>

<body>
   <?php include "nav.php"; ?>
    <div class="parralax-contact">
        <div class="container">
            <div class="alert alert-danger alert-dismissible fade show d-none">
                <button type="button" class="close" onclick="closeAlert();">&times;</button>
                <strong>Stop!</strong> Please fill out all of the fields correctly.
            </div>
            <div class="row">
                <div class="col">
                    <h2 style="text-align: center;">Contact Me!</h2>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <form name="contactForm" method="post" onsubmit="event.preventDefault(); validateContact();" action="<?php echo htmlentities($_SERVER['PHP_SELF']); ?>">
                        <div class="form-group">
                            <label for="date">Date</label>
                            <input type="date" class="form-control" id="date" name="date" aria-describedby="date">
                        </div>
                        <div class="form-group">
                            <label for="fname">First Name</label>
                            <input type="text" class="form-control" id="fname" name="fname" aria-describedby="firstName" placeholder="Enter First Name">
                        </div>
                        <div class="form-group">
                            <label for="lname">Last Name</label>
                            <input type="text" class="form-control" id="lname" name="lname" aria-describedby="lastName" placeholder="Enter Last Name">
                        </div>
                        <div class="form-group">
                            <label for="email">Email</label>
                            <input type="text" class="form-control" id="email" name="email" aria-describedby="email" placeholder="Email">
                        </div>
                        <div class="form-group">
                            <label for="phone">Phone Number</label>
                            <input type="text" class="form-control" id="phone" name="phone" aria-describedby="phone" placeholder="(XXX)-XXX-XXXX">
                        </div>
                        <div class="form-group">
                            <label for="company">Company (if applicable)</label>
                            <input type="text" class="form-control" id="company" name="company" aria-describedby="company" placeholder="Company Name">
                        </div>
                        <div class="form-group">
                            <label for="listReason">Reason for contact</label>
                            <select class="form-control" id="listReason" name="listReason">
                                <option value="1">I am a recruiter</option>
                                <option value="2">Looking for a contract worker</option>
                                <option value="3">General info</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="txtReasonDetail">Please provide a short message detailing your reason for
                                contact.</label>
                            <textarea class="form-control" id="txtReasonDetail" name="txtReasonDetail" aria-describedby="txtReasonDetail"
                                placeholder="Details..."></textarea>
                        </div>
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</body>

</html>

<?php
include "mydb.php";
$date = $fname = $lname = $email = $phone = $company = $contact_reason = $message = "";

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $date = $_POST["date"];
    $fname = cleanse_data($_POST["fname"]);
    $lname = cleanse_data($_POST["lname"]);
    $email = cleanse_data($_POST["email"]);
    $phone = cleanse_data($_POST["phone"]);
    $company = cleanse_data($_POST["company"]);
    $contact_reason = cleanse_data($_POST["listReason"]);
    $message = cleanse_data($_POST["txtReasonDetail"]);

    insert_to_contact_table($date, $fname, $lname, $email, $phone, $company, $contact_reason, $message);
}

function cleanse_data($data)
{
    return htmlspecialchars(stripslashes(trim($data)));
}

?>