function validateAbout() {
	if (document.getElementById("fname").value === "") {
		$('.alert').removeClass('d-none').addClass('show');
		document.getElementById("fname").classList.add("required-input");
	} else {
		document.getElementById("fname").classList.remove("required-input");
	}

	if (document.getElementById("lname").value === "") {
		$('.alert').removeClass('d-none').addClass('show');
		document.getElementById("lname").classList.add("required-input");
	} else {
		document.getElementById("lname").classList.remove("required-input");
	}

	if (document.getElementById("txtHobbies").value === "") {
		$('.alert').removeClass('d-none').addClass('show');
		document.getElementById("txtHobbies").classList.add("required-input");
	} else {
		document.getElementById("txtHobbies").classList.remove("required-input");
	}

	if (document.getElementById("txtMovies").value === "") {
		$('.alert').removeClass('d-none').addClass('show');
		document.getElementById("txtMovies").classList.add("required-input");
	} else {
		document.getElementById("txtMovies").classList.remove("required-input");
	}

	if (document.getElementById("txtFood").value === "") {
		$('.alert').removeClass('d-none').addClass('show');
		document.getElementById("txtFood").classList.add("required-input");
	} else {
		document.getElementById("txtFood").classList.remove("required-input");
	}

	if (document.getElementById("listDay").value === "") {
		$('.alert').removeClass('d-none').addClass('show');
		document.getElementById("listDay").classList.add("required-input");
	} else {
		document.getElementById("listDay").classList.remove("required-input");
	}

	if (document.querySelector('input[name=sports]:checked') === null) {
		$('.alert').removeClass('d-none').addClass('show');
		document.getElementById("sportsLabel").classList.add("required-input");
	} else {
		document.getElementById("sportsLabel").classList.remove("required-input");
	}

	if (document.querySelectorAll('input[name=games]:checked') === []) {
		$('.alert').removeClass('d-none').addClass('show');
		document.getElementById("checkboxGames").classList.add("required-input");
	} else {
		document.getElementById("checkboxGames").classList.remove("required-input");
	}
}

function validateContact() {
	fieldsValid = false;
	fieldsValidated = 0;
	for (i = 0; i < document.forms["contactForm"].getElementsByTagName("input").length; i++) {
		if (document.forms["contactForm"][i].type === "text" || document.forms["contactForm"][i].type === "date") {
			if (document.forms["contactForm"][i].value === "") {
				$('.alert').removeClass('d-none').addClass('show');
				document.forms["contactForm"][i].classList.add("required-input");
			} else {
				document.forms["contactForm"][i].classList.remove("required-input");
				fieldsValidated += 1;
			}
		}
	}

	if(fieldsValidated === document.forms["contactForm"].getElementsByTagName("input").length){
		document.forms["contactForm"].submit();
	}
}

function closeAlert() {
	$('.alert').addClass('d-none').removeClass('show');
}